import socks
import socket
import time
import os
socks.setdefaultproxy(socks.PROXY_TYPE_SOCKS5, "127.0.0.1", 8081)
socket.socket = socks.socksocket

import urllib, urllib2
import json
#list = json.loads(urllib2.urlopen("http://mapit.mysociety.org/areas/WMC.json").read())
datadir = os.path.join("..", "data", "const", "boundaries")
list = json.loads(open(os.path.join("..", "data", "const", "WMC.json")).read())
for id,info in list.items():
    if not "%s.geojson" % id in os.listdir(datadir):
        print "http://mapit.mysociety.org/area/%s.geojson" % id
        print urllib.urlretrieve("http://mapit.mysociety.org/area/%s.geojson" % id, os.path.join(datadir, "%s.geojson" % id))
        time.sleep(1)