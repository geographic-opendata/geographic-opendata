import sys,os
sys.path.append(os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from const.models import *

from django.contrib.gis.geos import GEOSGeometry
import django.contrib.gis.gdal.error
import re, json

datadir = os.path.join("..", "data", "const")
list = os.listdir(os.path.join(datadir, "boundaries"))
clist = json.loads(open(os.path.join(datadir, "WMC.json")).read())
for file in list:
    m = re.match("([0-9]+)\.geojson$", file)
    if m:
        mapitid = m.group(1)
        geojson = open(os.path.join(datadir, "boundaries", file), "r")
        name = clist[mapitid]["name"]
        try:
            boundary = GEOSGeometry(geojson.read())
            constituency = Constituency(mapitid=int(mapitid), name=name, boundary=boundary)
            constituency.save()
        except ValueError: pass
        except django.contrib.gis.gdal.error.OGRException: pass
