import sys,os
sys.path.append(os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from const.models import Constituency, Party

import csv, re

def onlyascii(char):
    if ord(char) > 127: return ''
    else: return char

datadir = os.path.join("..", "data", "const")
sheet = csv.reader(open(os.path.join(datadir, "mps.csv"),"r"))
header = sheet.next()
for personid,fname,lname,party,constituency,uri in sheet:
    name = filter(onlyascii, fname+" "+lname)
    constituency = filter(onlyascii, constituency)
    print constituency
    try:
        c = Constituency.objects.get(name=constituency)
        try:
            p = Party.objects.get(name=party)
        except Party.DoesNotExist:
            p = Party(name=party)
            p.save()
        c.mpname = name
        c.party = p
        c.save()
    except Constituency.DoesNotExist: pass
    print
    
sheet = csv.reader(open(os.path.join(datadir, "population.csv"),"r"))
header = sheet.next()
for row in sheet:
    name = row[1]
    total = row[2].replace(",", "")
    constituency = filter(onlyascii, name)
    try:
        c = Constituency.objects.get(name=constituency)
        c.population = int(total)
        c.save()
    except Constituency.DoesNotExist: pass
    