from django.contrib.gis.db import models

class Party(models.Model):
    name = models.CharField(max_length=100, unique=True)
    colour = models.CharField(max_length=10, null=True)
    def __unicode__(self):
        return self.name

class Constituency(models.Model):
    mapitid = models.IntegerField(unique=True)
    name = models.CharField(max_length=100, unique=True)
    boundary = models.GeometryField(spatial_index=True)
    mpname = models.CharField(max_length=100, null=True)
    party = models.ForeignKey(Party, null=True)
    population = models.IntegerField(null=True)
    rank = models.IntegerField(null=True)
    partyrank = models.IntegerField(null=True)
    objects = models.GeoManager()
