from django.conf.urls.defaults import *

urlpatterns = patterns('',
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': '/home/bjwebb/code/nhtg11/static'}),
    (r'^/?$', "const.views.index"),
    (r'^all/?$', "const.views.all"),
    (r'^search/?$', "const.views.search"),
    (r'^cbyll/(?P<ll>[0-9-\.,]+)$', "const.views.cbyll"),
    (r'^c/(?P<cid>[0-9]+)$', "const.views.c")
)