from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, Context, loader
from const.models import *
from police.models import Crime, CRIMETYPES
from django.contrib.gis.geos import GEOSGeometry, Point
from django.db.models import Count # Sum, Avg, Max, Min, 

def index(request):    
    t = loader.get_template("const/search.html")
    c = Context({
        "clist": ""
    })
    #c.update(csrf(request))
    return HttpResponse(t.render(c))
    
def all(request):
    cs = Constituency.objects.all()
    out= []
    #q = Constituency.objects.annotate(Count("location_set__crime"))
    #out.append( q[0] )
    #out.append( " " )
    parties = {}
    partytable = {}
    ctable = []
    for c in cs:
        #crime = c.location_set.aggregate(Count("crime"))['crime__count']
        crimes = Crime.objects.filter(location__point__within=c.boundary)      
        crime = crimes.count()
        if c.party:
            if not c.party.name in parties: parties[c.party.name] = crime
            else: parties[c.party.name] += crime
            if not c.party.name in partytable: partytable[c.party.name] = []
            partytable[c.party.name].append( (c.id, crime, c.name, c.mpname, c.population, c.party) )
        ctable.append( (c.id, crime, c.name, c.mpname, c.population, c.party) )
        
        #out.append( c.name + " " + str(crime) + " " + str(c.mpname) + " " + str(c.population)  + " " + str(c.party) )
    i=1
    sortedtable = []
    for row in sorted(ctable, key=lambda x: 1000000-x[1]):
        c = Constituency.objects.get(pk=row[0])
        c.rank = i
        c.save()
        sortedtable.append( (row[1], row[2], row[3], row[4], row[5]) )
        """try:
            out.append(" ".join(map(str, row)))
        except UnicodeEncodeError: pass"""
        i+=1
    
    for party,ptable in partytable.items():
        i=1
        for row in sorted(ptable, key=lambda x: 1000000-x[1]):
            c = Constituency.objects.get(pk=row[0])
            c.partyrank = i
            c.save()
            #out.append("<br></td><td style=\"vertical-align: top;\">".join(map(str, row)))
            i+=1
    
    t = loader.get_template("const/leaderboards.html")
    c = Context({
        "clist": sortedtable
    })
    #c.update(csrf(request))
    return HttpResponse(t.render(c))
    
def cbyll(request, ll):
    lat, lon = ll.split(",")
    point = Point(float(lat),float(lon))
    print point.x, point.y
    c = Constituency.objects.get(boundary__contains=point)
    return HttpResponseRedirect("/const/c/"+str(c.id))
    out = "<strong>" + str(c.id) + c.name + "</strong><br/>\n" + str(c.location_set.aggregate(Count("crime"))['crime__count']) + " " + str(c.mpname) + " " + str(c.population) + " " + str(c.party)
    out += "<br/><br/>\n\n"
    for loc in c.location_set.all():
        out += loc.name + " " + str(loc.easting) + " " + str(loc.northing) + "<br/>"
    return HttpResponse(out)

def search(request):
    if "q" in request.POST:
        q = request.POST["q"]
        cs = Constituency.objects.filter(name__icontains=q)
        out = ""
        for c in cs:
            return HttpResponseRedirect("/const/c/"+str(c.id))
            out += c.name+"<br/>\n"
        return HttpResponse(out)
    else:
        return HttpResponse("")

def c(request, cid):
    c = Constituency.objects.get(pk=cid)
    crimes = Crime.objects.filter(location__point__within=c.boundary)        
        #crimes.filter(type="ASB")
    crime = crimes.count()
    ctypes = []
    for crimetype in CRIMETYPES:
        ctypes.append( ( crimetype[1], crimes.filter(crimetype=crimetype[0]).count() ) )
        
    if c.party:
        partyname = c.party.name
        partycolour = c.party.colour
    else:
        partyname = "ERROR"
        partycolour = "000000"
    if c.population:
        per1000 = "%.2f" % ((1.0*crime/c.population)*1000)
    else: per1000 = "Err"
    t = loader.get_template("const/results.html")
    c = Context({
        "name": c.name,
        "mpname": c.mpname,
        "partyname": partyname,
        "population": c.population,
        "crime": crime,
        "per1000": per1000,
        "ctypes": ctypes,
        "rank": c.rank,
        "partyrank": c.partyrank,
        "partycolour": partycolour
    })
    #c.update(csrf(request))
    return HttpResponse(t.render(c))
    
