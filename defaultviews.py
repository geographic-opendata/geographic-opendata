from django.contrib.gis.geos import Point
from django.contrib.gis.gdal.envelope import Envelope
from django.http import HttpResponse
from django.contrib.gis.db.models import Count
import json

def show(request, q, pk, limit, format, M, fieldname, appname="transport"):
    if limit == None: limit = 100;
    objects = None
    if format == ".json" or format == "_cluster.json":
        out = []
    else:
        out = ""
    if pk:
        o = M.objects.get(pk=int(pk))
        for field in o._meta.fields:
            if format == ".json":
                pass
            else:
                out += "<li><strong>"+field.name+":</strong> "+field.value_to_string(o)+"</li>\n"
    else:
        if format == "_cluster.json":
            if 'west' in request.GET:
                minx = float(request.GET['west'])
                miny = float(request.GET['south'])
                maxx = float(request.GET['east'])
                maxy = float(request.GET['north'])
            else:
                minx = -9.05
                miny = 48.77
                maxx = 2.19
                maxy = 58.88
            aggregate = None
            if "agg" in request.GET:
                aggregate = request.GET["agg"]
            scale = Point(minx,miny).distance(Point(maxx,maxy))
            print scale
            deltax = (maxx - minx) / 10
            deltay = (maxy - miny) / 10
            gridminx = minx
            gridmaxx = None
            clusters = []
            while gridmaxx == None or gridmaxx < maxx:
                gridmaxx = gridminx + deltax
                gridminy = miny
                gridmaxy = None
                while gridmaxy == None or gridmaxy < maxy:
                    gridmaxy = gridminy + deltay
                    bbox = Envelope(gridminx,gridminy,gridmaxx,gridmaxy)
                    bounded = M.objects.filter(**{fieldname+"__intersects": bbox.wkt})
                    if aggregate:
                        count = bounded.aggregate(Count(aggregate))[aggregate+"__count"]
                    else:
                        count = bounded.count()
                    if count > 0:
                        clusters.append({fieldname:Point((gridminx+gridmaxx)/2, (gridminy+gridmaxy)/2), "count":count})
                    gridminy = gridmaxy
                gridminx = gridmaxx
            while len(clusters):
                cluster1 = clusters.pop()
                re = False
                for cluster2 in clusters:
                    if cluster1[fieldname].distance(cluster2[fieldname]) < (scale/7):
                        clusters.remove(cluster2)
                        if cluster2["count"] > cluster1["count"]:
                            cluster2["count"] += cluster1["count"]
                            clusters.append(cluster2)
                        else:
                            cluster1["count"] += cluster2["count"]
                            clusters.append(cluster1)
                        re = True
                        break
                if not re:
                    out.append({"lat":cluster1[fieldname].y, "lon":cluster1[fieldname].x, "count":cluster1["count"]})
        elif 'north' in request.GET:
            bbox = Envelope(float(request.GET['west']), float(request.GET['south']), float(request.GET['east']), float(request.GET['north']))
            objects = M.objects.filter(**{fieldname+"__intersects": bbox.wkt})[:limit]
        else:
            objects = M.objects.all()[:limit]
        if objects:
            for o in objects:
                if format == ".json":
                    d = {}
                    for field in o._meta.fields:
                        if field.__class__.__name__ == "PointField":
                            d["lat"] = field._get_val_from_obj(o).y
                            d["lon"] = field._get_val_from_obj(o).x
                        else:
                            d[field.name] = field.value_to_string(o)
                    out.append(d)
                else:
                    out += "<li><a href=\"/"+appname+"/"+q+"/"+str(o.pk)+"\">" + unicode(o) + "</a></li>\n"
    if format == ".json" or format == "_cluster.json":
        if "debug" in request.GET:
            return HttpResponse(json.dumps(out))
        else:
            return HttpResponse(json.dumps(out), mimetype="application/json")
    else:
        return HttpResponse("<ol>"+out+"</ol>")