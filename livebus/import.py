import sys,os
sys.path.append(os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from livebus.models import *
from transport.models import NaptanStop
import datetime

Route.objects.all().delete()
Journey.objects.all().delete()
StopTime.objects.all().delete()

def withindate(sd, ed):
    today = datetime.date.today()
    s = datetime.datetime.strptime(sd, "%Y%m%d").date()
    if ed == "00000000":
        return s <= today
    else:
        e = datetime.datetime.strptime(ed, "%Y%m%d").date()
        return s <= today and today <= e

dir = os.path.join("data", "GMPTE_CIF")
for f in os.listdir(dir):
    try:
        print f
        if f == "GM___17_.CIF":
            continue
        file = open(os.path.join(dir,f))
        
        header = file.readline()
        
        docdict = {}
        currentjourney = None
        route = None
        sid = 0
        for line in file:
            #print start_date, end_date
            code = line[:2]
            #print code
            if code == "ZD":
                start_date = line[2:10]
                end_date = line[10:18]
                if withindate(start_date, end_date):
                    input = True
                else:
                    input = False
            elif input and code == "ZS":
                try:
                    route = Route.objects.get(name=line[14:].strip(), number=line[10:14].strip())
                except Route.DoesNotExist:
                    route = Route()
                    route.name = line[14:].strip()
                    route.number = line[10:14].strip()
                    route.save()
            elif input and code == "QS":
                fields = {
                    "term_time": (1, 37),
                    "bank_holidays": (1, 38),
                    "route_number": (4, 39)
                }
                journey = Journey()
                for (field, (length, start)) in fields.items():
                    value = line[start-1:start+length-1].strip()
                    if field == "term_time" or field == "bank_holidays":
                        journey.__setattr__( field, value )
                journey.route = route
                #journey["days"] = []
                for i in range(30,37):
                    if line[i-1:i] == "1":
                        journey.__setattr__( Journey.DAYS[i-30], True )
                journey.save()
                currentjourney = journey
                sid = 0
            elif input and (code == "QO" or code == "QI" or code == "QT"):
                fields = {
                    "stopcode": (12, 3),
                    "time": (4, 15)
                }
                try:
                    stoptime = StopTime()
                    for (field, (length, start)) in fields.items():
                        value = line[start-1:start+length-1].strip()
                        if field == "stopcode":
                            stoptime.stop = NaptanStop.objects.get(atcocode=value)
                        elif field == "time":
                            #print value
                            stoptime.time = datetime.time(hour=int(value[:2]), minute=int(value[2:4]))
                    stoptime.sequenceid = sid
                    sid += 1
                    stoptime.journey = currentjourney
                    stoptime.save()
                except NaptanStop.DoesNotExist: continue
    except ValueError:
        continue
