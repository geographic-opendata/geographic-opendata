from django.contrib.gis.db import models
from transport.models import NaptanStop

class Route(models.Model):
    name = models.CharField(max_length=100)
    number = models.CharField(max_length=4)
    
    def __unicode__(self):
        return self.name

class Journey(models.Model):
    DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]
    monday = models.BooleanField()
    tuesday = models.BooleanField()
    wednesday = models.BooleanField()
    thursday = models.BooleanField()
    friday = models.BooleanField()
    saturday = models.BooleanField()
    sunday = models.BooleanField()
    
    term_time = models.CharField(max_length=1)
    bank_holidays = models.CharField(max_length=1)
    route = models.ForeignKey(Route, null=True)
    def __unicode__(self):
        return unicode(self.pk)
    objects = models.GeoManager()


class StopTime(models.Model):
    sequenceid = models.IntegerField()
    time = models.TimeField()
    journey = models.ForeignKey(Journey)
    stop = models.ForeignKey(NaptanStop)
    def __unicode__(self):
        return unicode(self.journey) + " " + unicode(self.sequenceid) + " " + unicode(self.time)
    objects = models.GeoManager()
