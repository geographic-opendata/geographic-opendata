from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    url(r'^$', 'livebus.views.home'),
    url(r'^(?P<q>(fakelive|journeys|stoptimes))(/(?P<pk>[0-9]+))?(?P<format>(|_cluster\.json|\.json|\.html))(\?.*)?$', 'livebus.views.show')

)

urlpatterns += staticfiles_urlpatterns()
