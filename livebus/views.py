from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, Context, loader
from django.contrib.gis.gdal.envelope import Envelope
from livebus.models import *
import defaultviews
import json, datetime

def total_seconds(td):
    return (td.microseconds + (td.seconds + td.days * 24 * 3600) * 10**6) / 10**6

def show(request, q, pk, limit=50, format=".html"):
    if q == "journeys": M = Journey
    elif q == "stoptimes": M = StopTime
    elif q == "fakelive":
        if format == ".json" or format == "_cluster.json":
            out = []
            now = datetime.datetime.now()
            journeys = Journey.objects.filter(**{
                Journey.DAYS[datetime.date.today().weekday()]:True,
                "stoptime__time__gt": (now - datetime.timedelta(minutes=5)).time(),
                "stoptime__time__lt": (now + datetime.timedelta(minutes=5)).time()
            })
            if 'north' in request.GET:
                bbox = Envelope(float(request.GET['west']), float(request.GET['south']), float(request.GET['east']), float(request.GET['north']))
                journeys = journeys.filter(stoptime__stop__location__intersects=bbox.wkt)
            journeys = journeys.distinct()
            i=0
            for journey in journeys:
                try:
                    stoptimes = StopTime.objects.filter(journey=journey)
                    nextstop = stoptimes.filter(time__gt=now.time()).order_by("sequenceid")[0]
                    prevstop = stoptimes.get(sequenceid= (nextstop.sequenceid - 1) )
                    prevstops = stoptimes.filter(time=prevstop.time).order_by("sequenceid")
                    n = datetime.datetime.combine(datetime.date.today(), nextstop.time)
                    p = datetime.datetime.combine(datetime.date.today(), prevstop.time)
                    if p > now: continue
                    if prevstops.count() > 1: # Account for consecutive stops with the same time
                        tot = total_seconds(n-p)
                        i  = int ( ( 1.0 * total_seconds(now-p) / tot ) * prevstops.count() )
                        prevstop = prevstops[i]
                        nextstop = stoptimes.get(sequenceid= (prevstop.sequenceid + 1) )
                        n = datetime.datetime.combine(datetime.date.today(), nextstop.time) + datetime.timedelta(seconds=(tot*i)/prevstops.count())
                        p = datetime.datetime.combine(datetime.date.today(), prevstop.time) + datetime.timedelta(seconds=(tot*(i+1))/prevstops.count())
                    nw = total_seconds(now-p)
                    pw = total_seconds(n-now)
                    d = {}
                    d["lat"] = ( nw*nextstop.stop.location.y + pw*prevstop.stop.location.y ) / (nw+pw)
                    d["lon"] = ( nw*nextstop.stop.location.x + pw*prevstop.stop.location.x ) / (nw+pw)
                    d["speed_lat"] = (nextstop.stop.location.y - prevstop.stop.location.y) / total_seconds(n-p)
                    d["speed_lon"] = (nextstop.stop.location.x - prevstop.stop.location.x) / total_seconds(n-p)
                    out.append(d)
                    i += 1
                    if i > limit:
                        break
                except StopTime.DoesNotExist:
                    continue
                except IndexError:
                    continue
            if "debug" in request.GET:
                return HttpResponse(json.dumps(out))
            else:
                return HttpResponse(json.dumps(out), mimetype="application/json")
        else:
            return HttpResponse("Json only.")
    return defaultviews.show(request, q, pk, limit, format, M, "stop__location", "livebus")

def home(request):
    t = loader.get_template("map.html")
    c = Context({
        "feedurl": "/livebus/fakelive.json?",
        "icon": "http://maps.google.com/mapfiles/ms/micons/bus.png",
        "update": True,
        "animate": True
    })
    return HttpResponse(t.render(c))