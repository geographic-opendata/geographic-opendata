from django.http import HttpResponse, HttpResponseRedirect
from django.template import Context, loader

def home(request):
    t = loader.get_template("home.html")
    c = Context({ })
    return HttpResponse(t.render(c))

def about(request, appname=None):
    if appname == "crime4u" or appname == "const":
        return HttpResponseRedirect("http://rewiredstate.org/projects/crime4u")
    else:
        return HttpResponse("Coming soon.")
