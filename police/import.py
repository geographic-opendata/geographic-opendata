import sys,os
sys.path.append(os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from police.models import *
from django.contrib.gis.geos import fromstr

import csv, re
datadir = os.path.join("data", "csv", "2011-02")

skip = 0
child_pid = -1
for filename in os.listdir(datadir): #["2011-02-dyfed-powys-street.csv"]: #
    if skip <= 0:
        if child_pid != 0:
            child_pid = os.fork()
        if child_pid == 0:
            sheet = csv.reader(open(os.path.join(datadir, filename),"r"))
            print filename
            header = sheet.next()
            
            m = re.match("[0-9]{4}-[0-9]{2}-(.*)-(neighbourhood|street).csv$", filename)
            if m:
                urlname = m.group(1)
                force = None
                if m.group(2) == "neighbourhood":
                    for date, forcename, code, total, b, asb, r, vh, vi, o in sheet:
                        if not force:
                            try:
                                force = Force.objects.get(urlname=urlname)
                            except Force.DoesNotExist:
                                force = Force(name=forcename, urlname=urlname)
                                force.save()
                        try:
                            neighbourhood = Neighbourhood.objects.get(force=force, code=code)
                        except Neighbourhood.DoesNotExist:
                            neighbourhood = Neighbourhood(force=force, code=code)
                            neighbourhood.save()
                        year,month = date.split("-")
                        for (crimetype, number) in ("B", b), ("ASB", asb), ("R", r), ("VH", vh), ("VI", vi), ("O", o):
                            nhstat = NeighbourhoodStat(year=year, month=month, neighbourhood=neighbourhood, crimetype=crimetype, number=number)
                            nhstat.save()
                    
                else:
                    import time
                    time.sleep(10)
                    for date, reportedby, forcename, easting, northing, locationname, type, context in sheet:
                        if not force:
                            try:
                                force = Force.objects.get(urlname=urlname)
                            except Force.DoesNotExist:
                                force = Force(name=forcename, urlname=urlname)
                                force.save()
                        point = fromstr("SRID=27700;POINT(%s %s)" % (easting, northing))
                        point.transform(4326)
                        try:
                            location = Location.objects.get(name=locationname, force=force, point=point)
                        except Location.DoesNotExist:
                            location = Location(name=locationname, force=force, point=point)
                            location.save()
                        year,month = date.split("-")
                        ctmap = {
                            "Burglary": "B",
                            "Anti-social behaviour": "ASB",
                            "Robbery": "R",
                            "Vehicle crime": "VH",
                            "Violent crime": "VI",
                            "Other crime": "O"
                        }
                        crime = Crime(year=year, month=month, reportedby=reportedby, location=location, context=context, crimetype=ctmap[type])
                        
                        crime.save()
                break
        else:
            #skip = 0
            pass
    #else:
    #    skip -= 1
