from django.contrib.gis.db import models

CRIMETYPES = (
    ("B", "Burglary"),
    ("ASB", "Anti-social behaviour"),
    ("R", "Robbery"),
    ("VH", "Vehicle crime"),
    ("VI", "Violent crime"),
    ("O", "Other")
)

class Force(models.Model):
    name = models.CharField(max_length=100)
    urlname = models.CharField(max_length=100)
    def __unicode__(self): return self.name

class Neighbourhood(models.Model):
    force = models.ForeignKey(Force)
    code = models.CharField(max_length=50)
    def __unicode__(self): return unicode(self.force) + u" - " + self.code
    
class NeighbourhoodStat(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()
    neighbourhood = models.ForeignKey(Neighbourhood)
    crimetype = models.CharField(max_length=3, choices=CRIMETYPES)
    number = models.IntegerField()
    def __unicode__(self): return unicode(self.neighbourhood) + u" - " + self.crimetype
    
class Location(models.Model):
    name = models.CharField(max_length=100)
    force = models.ForeignKey(Force)
    point = models.PointField(spatial_index=True)
    objects = models.GeoManager()
    def __unicode__(self): return unicode(self.force) + u" - " + self.name

class Crime(models.Model):
    year = models.IntegerField()
    month = models.IntegerField()
    reportedby = models.CharField(max_length=100)
    crimetype = models.CharField(max_length=3, choices=CRIMETYPES)
    location = models.ForeignKey(Location)
    context = models.CharField(max_length=1000)
    objects = models.GeoManager()
    def __unicode__(self): return unicode(self.location) + u" - " + self.crimetype
