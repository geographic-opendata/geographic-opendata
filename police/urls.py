from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^(?P<q>(forces|locations|crimes|nh|nhs))(/(?P<pk>[0-9]+))?(?P<format>(|_cluster\.json|\.json|\.html))(\?.*)?$', 'police.views.show'),
    url(r'^map', 'police.views.map')
)