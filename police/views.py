from police.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, Context, loader
import defaultviews

def show(request, q, pk, limit=50, format=".html"):
    if q == "forces": M = Force
    elif q == "locations": M = Location
    elif q == "crimes": M = Crime
    elif q == "nh": M = Neighbourhood
    elif q == "nhs": M = NeighbourhoodStat
    return defaultviews.show(request, q, pk, limit, format, M, "point")
    
def map(request):
    t = loader.get_template("map.html")
    if "cluster" in request.GET:
        c = Context({
            "feedurl": "/police/locations_cluster.json?agg=crime&",
            "cluster": True
        })
    else:
        c = Context({
            "feedurl": "/transport/locations.json?"
        })
    #c.update(csrf(request))
    return HttpResponse(t.render(c))
    return HttpResponse("")
