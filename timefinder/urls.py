from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'timefinder.views.index'),
    url(r'^(manchester|london)/$', 'timefinder.views.location_index'),
    url(r'^(manchester|london)/search$', 'timefinder.views.search'),
    url(r'^(manchester|london)/journey$', 'timefinder.views.journey'),
    url(r'^(manchester|london)/journey_map$', 'timefinder.views.journey_map'),
    url(r'^(manchester|london)/journey_map.json$', 'timefinder.views.journey_map_json'),
    url(r'^(manchester|london)/stop$', 'timefinder.views.stop'),
)

urlpatterns += staticfiles_urlpatterns()