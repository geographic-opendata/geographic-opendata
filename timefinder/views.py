from livebus.models import *
from transport.models import NaptanStop
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, Context, loader
import json
import datetime

homeurl = "/timefinder/"

def index(request):
    t = loader.get_template("timefinder/index.html")
    c = Context({ })
    return HttpResponse(t.render(c))
    
def location_index(request, location):
    t = loader.get_template("timefinder/location_index.html")
    c = Context({ "location": location })
    return HttpResponse(t.render(c))
    
def search(request, location):
    routes = Route.objects.filter(number=request.GET["search"])
    t = loader.get_template("timefinder/search.html")
    print routes
    c = Context({
        "search": request.GET["search"],
        "location": location,
        "routes": routes,
        "home": homeurl
    })
    return HttpResponse(t.render(c))
    
def journey(request, location):
    route = Route.objects.get(pk=request.GET["route"])
    stops = NaptanStop.objects.filter(stoptime__journey__route=route).distinct()
    print stops
    t = loader.get_template("timefinder/journey.html")
    c = Context({
        "route": route,
        "stops": stops,
        "location": location,
    })
    return HttpResponse(t.render(c))
    
def journey_map(request, location):
    t = loader.get_template("map.html")
    c = Context({
        "feedurl": "journey_map.json?route=%s&" % request.GET["route"],
        "static": True,
        "click": True,
        "home": homeurl
    })
    return HttpResponse(t.render(c))
    
def journey_map_json(request,location):
    route = Route.objects.get(pk=request.GET["route"])
    out = []
    stops = NaptanStop.objects.filter(stoptime__journey__route=route).distinct()
    for stop in stops:
        d = {}
        d["lat"] = stop.location.y
        d["lon"] = stop.location.x
        d["url"] = "stop?route=%d&stop=%d" % (route.pk, stop.pk)
        out.append(d)
    return HttpResponse(json.dumps(out))
    
def stop(request, location):
    route = Route.objects.get(pk=request.GET["route"])
    stop = NaptanStop.objects.get(pk=request.GET["stop"])
    stoptimes = StopTime.objects.filter(stop=stop)
    stoptimes = stoptimes.filter(journey__route=route)
    if "day" in request.GET:
        dayno = int(request.GET["day"])
    else:
        dayno = datetime.date.today().weekday()
    dayname = Journey.DAYS[dayno]
    stoptimes = stoptimes.filter(**{"journey__"+dayname: True}).order_by("time")
    t = loader.get_template("timefinder/stop.html")
    shortday = ["Mon", "Tues", "Wed", "Thurs", "Fri", "Sat", "Sun"]
    c = Context({
        "route": route,
        "stop": stop,
        "stoptimes": stoptimes,
        "location": location,
        "home": homeurl,
        "dayname": dayname.title(),
        "prevday": (dayno-1)%7,
        "nextday": (dayno+1)%7,
        "prevdayname": shortday[(dayno-1)%7],
        "nextdayname": shortday[(dayno+1)%7]
    })
    return HttpResponse(t.render(c))