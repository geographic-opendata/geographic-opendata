import sys,os
sys.path.append(os.path.abspath(".."))
os.environ["DJANGO_SETTINGS_MODULE"] = "settings"
from transport.models import *
from django.contrib.gis.geos import fromstr

import csv, re
def unicode_csv_reader(utf8_data, dialect=csv.excel, **kwargs):
    csv_reader = csv.reader(utf8_data, dialect=dialect, **kwargs)
    for row in csv_reader:
        yield [unicode(cell, "latin-1") for cell in row]

datadir = os.path.join("..", "data", "NaPTANcsv")
sheet = unicode_csv_reader(open(os.path.join(datadir, "Stops.csv"),"r"))
header = sheet.next()
i=0
for row in sheet:
    i += 1
    if i < 268000: continue
    if row[42] == "act":
        location = fromstr("SRID=27700;POINT(%s %s)" % (row[27], row[28]))
        location.transform(4326)
        try: stop = NaptanStop.objects.get(atcocode=row[0])
        except NaptanStop.DoesNotExist: stop = NaptanStop(atcocode=row[0])
        stop.naptancode = row[1]
        stop.name = row[4]
        stop.shortname = row[6]
        stop.landmark = row[8]
        stop.street = row[10]
        stop.bearing = row[16]
        stop.location = location
        stop.save()
    
    if (i % 1000) == 0:
        print i
