from django.contrib.gis.db import models

# Create your models here.
class NaptanStop(models.Model):
    atcocode = models.CharField(max_length=20, unique=True)
    naptancode = models.CharField(max_length=20)
    location = models.PointField(spatial_index=True)
    name = models.CharField(max_length=100)
    shortname = models.CharField(max_length=100)
    landmark = models.CharField(max_length=100)
    street = models.CharField(max_length=100)
    bearing = models.CharField(max_length=2)
    objects = models.GeoManager()
    def __unicode__(self): return self.name + "("+self.atcocode+")"
