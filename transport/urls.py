from django.conf.urls.defaults import patterns, include, url

urlpatterns = patterns('',
    url(r'^(?P<q>(naptanstops))(/(?P<pk>[0-9]+))?(?P<format>(|_cluster\.json|\.json|\.html))(\?.*)?$', 'transport.views.show'),
    url(r'^map', 'transport.views.map')
)