from transport.models import *
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext, Context, loader
import defaultviews

def show(request, q, pk, limit=50, format=".html"):
    if limit == None: limit = 100;
    if q == "naptanstops": M = NaptanStop
    return defaultviews.show(request, q, pk, limit, format, M, "location")
    
def map(request):
    t = loader.get_template("map.html")
    if "cluster" in request.GET:
        c = Context({
            "feedurl": "/transport/naptanstops_cluster.json?",
            "cluster": True
        })
    else:
        c = Context({
            "feedurl": "/transport/naptanstops.json?"
        })
    #c.update(csrf(request))
    return HttpResponse(t.render(c))
    return HttpResponse("")