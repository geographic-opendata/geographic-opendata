from django.conf.urls.defaults import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'meta.views.home'),
    url(r'^([a-z0-9]+)/about$', 'meta.views.about'),
    url(r'^transport/', include('geo.transport.urls')),
    url(r'^police/', include('geo.police.urls')),
    url(r'^(crime4u|const)/', include('geo.const.urls')),
    url(r'^livebus/', include('geo.livebus.urls')),
    url(r'^timefinder/', include('geo.timefinder.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)

urlpatterns += staticfiles_urlpatterns()
